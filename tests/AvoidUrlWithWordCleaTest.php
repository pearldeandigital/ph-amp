<?php

namespace Tests;

use Packedhouse\Amp\AmpPost;
use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class AvoidUrlWithWordCleaTest extends TestCase
{

    public function testAvoidUrlWithWordClea()
    {
        $post = $this->getPost($this->getUrlWithWordClea());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeInvalidAttributes', [$post['content']['formatted'], 'packed.house']);

        $this->assertEquals($this->getUrlWithWordCleaWithoutRemoving(), $formatted);
    }

    private function getUrlWithWordCleaWithoutRemoving()
    {
        return '<a href="http://www.brownthomas.com/storage+cleaning/wine-rack/invt/64x5620xccbwr&amp;bklist=icat,4,living,living-trends,irish-designers-living">link</a>';
    }

    private function getUrlWithWordClea()
    {
        return '<a href="http://www.brownthomas.com/storage+cleaning/wine-rack/invt/64x5620xccbwr&amp;bklist=icat,4,living,living-trends,irish-designers-living">link</a>';
    }
}