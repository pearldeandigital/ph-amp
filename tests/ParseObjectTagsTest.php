<?php

namespace Tests;

use Packedhouse\Amp\AmpPost;
use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseObjectTagsTest extends TestCase
{

    public function testObjectTagsAreNotAllowed()
    {
        $post = $this->getPost($this->getObject());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['packed.house']);

        $formatted = $this->invokeMethod($transformer, 'removeObjectTags', [$post['content']['formatted'], 'packed.house']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getObject()
    {
        return '<object width=486 height=657 src="packed.house"></object>';
    }
}
