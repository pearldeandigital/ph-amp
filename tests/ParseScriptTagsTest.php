<?php

namespace Tests;

use Packedhouse\Amp\AmpPost;
use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseScriptTagsTest extends TestCase
{

    public function testScriptTagsAreNotAllowed()
    {
        $post = $this->getPost($this->getScript());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'removeScriptTags', [$post['content']['formatted'], 'packed.house']);

        $this->assertEquals('<!-- some html comment -->', $formatted);
    }

    private function getScript()
    {
        return '<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><script type="text/javascript">
            googletag.cmd.push(function() {
                googletag.display(\'div-gpt-top\');
            });
        </script><!-- some html comment -->';
    }
}
