<?php
namespace Tests;

use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
 * Embedly card quote formatter test
 */
class EmbedlyCardQuoteTest extends TestCase
{
    public function testEmbedlyCardQuoteShouldBeReplaced()
    {
        $post = $this->getPost($this->getEmbedlyBlockquote());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parseEmbedlyCardQuote', [$post['content']['formatted'], 'www.packed.house']);

        $this->assertEquals($this->getBlockquoteFormatted(), $formatted);
    }

    private function getEmbedlyBlockquote()
    {
        return '<blockquote class="embedly-card" data-card-key="b161a5a96b0b41c297370952f4888bbc" data-card-type="article-full">'.
                '<h4><a href="https://www.buzz.ie/4-predictions-for-the-game-of-thrones-spin-offs/">'.
                '4 predictions for the Game of Thrones spin-offs | Buzz.ie</a></h4>'.
                '</blockquote>';
    }

    private function getBlockquoteFormatted()
    {
        return '<strong>Some elements are not supported yet for this mobile version. </strong>'.
        '<p><a href="www.packed.house">You can see the full post content here.</a></p>';
    }
}