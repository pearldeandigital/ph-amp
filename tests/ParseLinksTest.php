<?php

namespace Tests;

use Packedhouse\Amp\AmpPost;
use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseLinksTest extends TestCase
{

    public function testParseLinkTest()
    {
        $post = $this->getPost($this->getLink());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'cleanLinks', [$post['content']['formatted'], 'packed.house']);

        $this->assertEquals($this->getLinkFormatted(), $formatted);
    }

    private function getLinkFormatted()
    {
        return '<a href="packed.house"  >';
    }

    private function getLink()
    {
        return '<a href="packed.house" target="_hplink" alt="some alt tag">';
    }
}
