<?php

namespace Tests;

use Packedhouse\Amp\AmpPost;
use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParseMapTagsTest extends TestCase
{

    public function testMapTagsAreNotAllowed()
    {
        $post = $this->getPost($this->getMap());

        $transformer = new PublisherPlusTransformer($post);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['packed.house']);

        $formatted = $this->invokeMethod($transformer, 'removeMapTags', [$post['content']['formatted'], 'packed.house']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getMap()
    {
        return '<map width=486 height=657 src="packed.house"></map>';
    }
}
