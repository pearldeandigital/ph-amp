<?php

namespace Tests;

use Packedhouse\Amp\AmpPost;
use Packedhouse\Amp\Transformers\PublisherPlusTransformer;

/**
* AMP formatter test
*/
class ParsePollDaddyScriptsTest extends TestCase
{

    public function testPollDaddyEmbedsAreNotSupported()
    {
        $post = $this->getPost($this->getScript());

        $transformer = new PublisherPlusTransformer($post);

        $formatted = $this->invokeMethod($transformer, 'parsePooldaddyScripts', [$post['content']['formatted'], 'packed.house']);

        $unsupportedContent = $this->invokeMethod($transformer, 'getUnsupportedContent', ['packed.house']);

        $this->assertEquals($unsupportedContent, $formatted);
    }

    private function getScript()
    {
        return '<script src="http://static.polldaddy.com/p/9440739.js" type="text/javascript" charset="utf-8"></script>';
    }
}
