<?php

namespace Packedhouse\Amp;

use Illuminate\Support\ServiceProvider;
use Packedhouse\Amp\AmpPost;

/**
 * FormatterServiceProvider
 */
class AmpPostServiceProvider extends ServiceProvider
{

    /**
     * Register AmpPost service provider
     */
    public function register()
    {
        $this->app->bind('amp', function ($app) {
            return new AmpPost();
        });
    }
}
